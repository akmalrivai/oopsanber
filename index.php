<?php
    require_once("animals.php");
    require_once("ape.php");
    require_once("frog.php");

    $animal = new Animals("Shaun",2);
    echo "Nama hewan ini adalah $animal->name " . " dan punya kaki berjumlah $animal->leg" . " dan salah satu hewan berdarah dingin itu adalah $animal->coldBlooded <br><br>";

    $ape = new Ape("Sungokong",2);
    echo "Nama hewan ini adalah $ape->name " . " dan punya kaki berjumlah $ape->leg" . " dan salah satu hewan berdarah dingin itu adalah $ape->coldBlooded <br><br>";
    echo yell();
    echo "<br><br>";
    

    $frog = new Frog("Kodok",4);
    echo "Nama hewan ini adalah $frog->name " . " dan punya kaki berjumlah $frog->leg" . " dan salah satu hewan berdarah dingin itu adalah $frog->coldBlooded <br><br>";
    echo jump();

?>